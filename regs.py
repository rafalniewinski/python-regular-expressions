import re

# special metacharacters
# . ^ $ * + ? { } [ ] \ | ( )

# . It matches anything except a newline character
# ^ Matches at the beginning of lines
# * specifies that the previous character can be matched zero or more times
# + matches one or more times ca+t match cat and caat but not ct * match also ct
# ? marking something as being optional home-?brew match homebrew and home-brew
# {} using for building regular regs
# [] using for building regular regs
# () using for building regular regs
# \ using for special characters like \w \s \d
# | or

# More special metacharacters

# [a-z] catch every letter between a-zw
# [abc] catch a b c letter
# [^5] catch every number except 5
# [0-9] catch every number between 0-9
# \w catch will match all the characters marked as letters in the Unicode db
# \[\ catch special characters

# \d Matches any decimal digit; this is equivalent to the class [0-9].
# \D Matches any non-digit character; this is equivalent to the class [^0-9].

# \s Matches any whitespace character
# \S Matches any non-whitespace character

# \w Matches any alphanumeric character
# \W  Matches any non-alphanumeric character

# Methods on compile object

# match() Determine if the RE matches at the beginning of the string.
# search() Scan through a string, looking for any location where this RE matches
# findall() Find all substrings where the RE matches, and returns them as a list
# finditer() Find all substrings where the RE matches, and returns iterator

# methods of match objects

# group() Return the string matched by the RE
# start() Return the starting position of the match
# end() Return the ending position of the match
# span() Return a tuple containing the (start, end) positions of the match

# Modifying Strings

# split() Split the string into a list, splitting it wherever the RE matches
# sub() Find all substrings where the RE matches, and replace them with
# a different string
# subn() Does the same thing as sub(), but returns the new string and the number
# of replacements

# Example 1

test_string = "xa1 1 xsad2 31 another lorem ispm"
p = re.compile('\d+')
new_string = p.sub('', test_string)
print(new_string)

# Example 2

randStr = "\\random this is my random string"
p = re.compile('\\random')
print("Our string:", p.search(randStr))
p = re.compile(r'\\random')
print("Our string:", p.search(randStr))

# Example 3

# ---------- Problem ----------
# Create a Regex that matches email addresses from a list
# 1. 1 to 20 lowercase and uppercase letters, numbers, plus ._%+-
# 2. An @ symbol
# 3. 2 to 20 lowercase and uppercase letters, numbers, plus .-
# 4. A period
# 5. 2 to 3 lowercase and uppercase letters

emailList = "db@aol.com m@.com @apple.com db@.com"

print("Email Matches :",
      len(re.findall("[\w._%+-]{1,20}@[\w.-]{2,20}.[A-Za-z]{2,3}",
                     emailList)))

# Example 4

# Check for valid first and last name with a space
if re.search("\w{2,20}\s\w{2,20}", "Toshio Muramatsu"):
    print("It is a valid full name")

# Example 5

phNum = "412-555-333"

# Check if it is a phone number
if re.search("\d{3}-\d{3}-\d{3}", phNum):
    print("It is a phone number")
